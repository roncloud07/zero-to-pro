<form action="" method="post">
	{{ csrf_field() }}
	<label for="">Name</label>
	<input type="text" name="name">

	<label for="">Url</label>
	<input type="text" name="url">

	<br>

	<label for="">Member</label><br>
	@foreach ($members as $member)
		<input type="radio" name="member_id" value="{{ $member->id }}"> {{ $member->name }} <br>
	@endforeach
	<input type="submit">
</form>
