<form action="" method="post">
	{{ csrf_field() }}
	<label for="">Name</label>
	<input type="text" name="name" value="{{ $member->name }}">

	<label for="">Email</label>
	<input type="text" name="email" value="{{ $member->email }}">

	<label for="">Password</label>
	<input type="password" name="password" value="{{ $member->password }}">
	<input type="submit">
</form>
<br>
<ul>
@foreach ($documents as $document)
	<li>{{ $document->name }} {{ $document->member->name }}</li>
@endforeach
</ul>
