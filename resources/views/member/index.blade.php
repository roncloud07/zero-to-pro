<h1>{{ $title }}</h1>
<ul>
@foreach($members as $member)
	<li><a href="{{ url('member/edit/' . $member->id) }}">{{ $member->name }} - {{ $member->email }}</a> - <a href="{{ url('member/delete/' . $member->id) }}" color="red">delete</a></li>
@endforeach
</ul>
