<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PolymorphicSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->unsignedInteger('person_id');
            $table->string('person_type');
        });

        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        Schema::create('lecturers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('members', function (Blueprint $table) {
            $table->dropColumn('person_id');
            $table->dropColumn('person_type');
        });

        Schema::drop('students');
        Schema::drop('lecturers');
    }
}
