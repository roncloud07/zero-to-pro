<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Member;
use \Input;
use \Request;

class MemberController extends Controller
{
    public function index()
    {
        $members = Member::all();
        $title   = 'Member Data';

        return view('member.index', compact('members', 'title'));
    }

    public function add()
    {
        if (Request::isMethod('get')) {
            // tampilkan form

            return view('member.add');

        } else {
            // simpan member baru
            Member::create(Input::only('name', 'email', 'password'));

            return redirect('member');
        }
    }

    public function edit($id)
    {
        $member = Member::find($id);
        if (Request::isMethod('get')) {
            // tampilkan form
            $documents = $member->documents()->get();

            return view('member.edit', compact('member', 'documents'));

        } else {
            // simpan member baru
            $member->update(Input::only('name', 'email', 'password'));

            return redirect('member');
        }
    }

    public function delete($id)
    {
        $member = Member::find($id);
        $member->delete();

        return redirect('member');
    }
}
