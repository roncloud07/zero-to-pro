<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Document;
use App\Models\Member;
use \Input;
use \Request;

class DocumentController extends Controller
{
    public function index()
    {
        $documents = Document::get();

        return view('document.index', compact('documents'));
    }

    public function add()
    {
        if (Request::isMethod('get')) {
            // tampilkan form
            $members = Member::get();

            return view('document.add', compact('members'));

        } else {
            // simpan document baru
            $document = Document::create(Input::only('name', 'url', 'member_id'));
            // $member_id = Input::get('member_id');
            // $member    = Member::find($member_id);

            // $document->member()->associate($member)->save();

            return redirect('document');
        }
    }

}
