<?php

use App\Models\Asset;
use App\Models\Lecturer;
use App\Models\Member;
use App\Models\Student;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

get('admin', ['middleware' => 'auth', 'uses' => 'AdminController@dashboard']);
get('login', function () {
    return view('login');
});
post('login', function () {
    $success = Auth::attempt(Input::only('email', 'password'));
    if ($success) {
        echo 'sucess';
        return redirect()->intended();
    }

    return redirect()->back();
});

Route::any('member', 'MemberController@index');
Route::any('member/add', 'MemberController@add');
Route::any('member/edit/{id}', 'MemberController@edit');
Route::any('member/delete/{id}', 'MemberController@delete');

Route::any('document', 'DocumentController@index');
Route::any('document/add', 'DocumentController@add');
Route::any('document/edit/{id}', 'DocumentController@edit');
Route::any('document/delete/{id}', 'DocumentController@delete');

Route::get('has', function () {
    $memberThatHasDocuments = Member::has('documents')->get();
    dd($memberThatHasDocuments);
});

Route::get('eager', function () {
    $members = Member::get();

    $members->load('documents');

    foreach ($members as $member) {
        $member->documents;
    }

});

Route::get('morph', function () {
    // Student::create(['name' => 'Mahasiswa']);
    // Lecturer::create(['name' => 'Dosen']);
    $mahasiswa = Student::first();
    $dosen     = Lecturer::first();

    $saya = Member::where('name', 'Saya')->first();
    $kamu = Member::where('name', 'Kamu')->first();

    $dosen->member()->save($saya);
    $mahasiswa->member()->save($kamu);

    dump($kamu->person);
    dump($saya->person);
});

Route::get('coba_asset', function () {
    // Asset::create(['name' => 'meja']);
    // Asset::create(['name' => 'kursi']);
    // Asset::create(['name' => 'lemari']);

    $member = Member::first();
    $assets = Asset::get();

    $array      = [1, 2, 3, 4];
    $collection = collect($array);
    $reverse    = $collection->reverse();

    $asset_ids = $assets->pluck('id')->all();

    // dd($asset_ids);
    // $asset = Asset::first();

    $member->assets()->sync($asset_ids);
    // $member->assets()->detach($asset);
    // $member->assets()->attach($asset);

    dump($member->assets);

    $user->person;
    // Mahasiswa
    // Dosen

    dump(Asset::get()->toArray());
    dump(Member::get()->toArray());
});
