<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecturer extends Model
{
    protected $fillable = ['name'];
    public $timestamps  = false;

    public function member()
    {
        return $this->morphOne('App\Models\Member', 'person');
    }
}
