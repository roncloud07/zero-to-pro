<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $table    = 'documents';
    protected $fillable = [
        'name',
        'url',
        'member_id',
    ];

    public function member()
    {
        return $this->belongsTo('App\Models\Member');
    }
}
