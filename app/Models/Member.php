<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    public function scopeMale($query)
    {
        return $query->where('gender', 'P');
    }

    public static function getLastId()
    {
        return static::orderBy('id', 'desc')->first()->id;
    }

    public function documents()
    {
        return $this->hasMany('App\Models\Document');
    }

    public function document()
    {
        return $this->hasOne('App\Models\Document');
    }

    public function assets()
    {
        return $this->belongsToMany('App\Models\Asset');
    }

    public function person()
    {
        return $this->morphTo();
    }
}
